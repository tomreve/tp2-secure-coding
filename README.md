#Question 1: please write a small paper about that naming convention.
The common Rest naming convention is to use nouns to name URI. We dont use for example mysite/GetUser but mysite/user as it must be manage within the HTTP request. 

Name must be clear and intuitive, there must be no abridged name.

The character '/' is used to navigate through the hierarchy of our api.
There must be no / at the end to be clearer.

Words are separated with hyphen like in this example: mysite/first-name

URI must be in lowercase. URI with capital letter might cause errors.

You should avoid special character as much as possible.

You should avoid using file extensions in URI. 

#Question 2: considering they use REST naming convention, what would do POST /web-api/users and POST /web-api/sessions endpoints?

POST /web-api/users va nous permettre de créer un nouvel user pour notre web-api.
POST /web-api/sessions va nous permettre de créer une nouvelle session pour notre web-api.